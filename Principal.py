# -*- coding: utf-8 -*-

from UiPrincipal import Ui_MainWindow
from PyQt4 import QtCore, QtGui
from ChatAPIBind import Chat
from AddContact import AddContact
from ChatView import ChatView
import ContactList
import time
import Queue
from threading import Thread

def _threadCheckIncoming(args):
		while (not args._exitThread):
			while (not args.core.connected()):
				args.core.connect()
				args.core.relogin()
				time.sleep(1.5)
			args.emit(QtCore.SIGNAL("checkAndProcessIncomingData()"))
			time.sleep(0.5)
		args._exited = True

class MainWindow(QtGui.QMainWindow):

        _exited = False
        _exitThread = False
	__contact_info = {}
	__contact_actual = None
	__login_window = None
	__msgid_last = None
	__msgid_indx = 0
	__thread = None
	queueData = Queue.Queue()

	def __init__(self,login):
		self.__login_window = login
		QtGui.QMainWindow.__init__(self);
		self.core = Chat()

		self.ui = Ui_MainWindow();
		self.ui.setupUi(self);

		self.ui.lineEdit_input.setDisabled(True)

		self.connect(self.ui.actionQuit, QtCore.SIGNAL('clicked()'), QtCore.SLOT('close()'))
		self.connect(self.ui.actionAdd, QtCore.SIGNAL('triggered(bool)'), self.addContactDialog)
		self.connect(self.ui.actionLogout, QtCore.SIGNAL('triggered(bool)'), self.logout)
		self.connect(self.ui.lineEdit_input, QtCore.SIGNAL('returnPressed()'), self.messageEntered)
		self.connect(self.ui.listWidget_contacts, QtCore.SIGNAL('itemClicked(QListWidgetItem *)'), self.clickContact)
		self.connect(self,QtCore.SIGNAL("checkAndProcessIncomingData()"),self.checkAndProcessIncomingData)
		self.__thread = Thread(target = _threadCheckIncoming,args = [self])
		self.__thread.start()	
	
	@QtCore.pyqtSlot()
	def checkAndProcessIncomingData(self):
		while (not self.queueData.empty()):
			data = self.queueData.get()
			if (data['type'] == "sendmessage"):
				args = data['args']
				self.core.sendmessage(args['to'],args['text'], args['id'])
		while (self.core.pop()):
			# Incoming message:
			if (self.core.gettag() == "message"):
				sender = self.core.getattribute("from").split("@")[0]
				msgid = self.core.getattribute("id")
				type_ = self.core.getattribute("type")
				if (self.core.gochild("notify")):
					name = self.core.getattribute("name")
					if (not self.core.goparent()):
						continue
				else:
					name = sender
				
				# Ununsed right now:
				#time = self.core.getattribute("t")
				
				# Incoming message is from new contact, add it:
				if (not sender in self.__contact_info):
					self.addContact({'id' : sender , 'nick' : name})
				# Now, if message is of type text, append it:
				if (type_ == "chat"):
					if (self.core.gochild("body")):
						content = self.core.getvalue()
						# Find ChatView of this contact:
						c = self.__contact_info[sender]
						c.appendMessage(msgid,{'type' : "text", 'text' : content, 'owner-name' : name})

	def addContact(self,contact):
		contact["remove"] = False
		self.ui.listWidget_contacts.updateContact(contact)
		self.__contact_info[contact['id']] = ChatView(contact['nick'])
		self.ui.chatStack.addWidget(self.__contact_info[contact['id']])
		
		self.__contact_actual = contact['id']
		self.ui.chatStack.setCurrentWidget(self.__contact_info[contact['id']])
		self.ui.lineEdit_input.setDisabled(False)

	def addContactDialog(self,checked):
		contact = AddContact().exec_()
		if (contact != None):
			self.addContact(contact)
	
			
	def clickContact(self,contact):
		self.__contact_actual = str(contact.data(ContactList.ROLE).toString())
		self.ui.chatStack.setCurrentWidget(self.__contact_info[str(contact.data(ContactList.ROLE).toString())])
	
	def logout(self):
		self.__login_window.show()
		self.hide()

	def generateMsgid(self):
		msgid = str(int(time.time()))
		if (self.__msgid_last == msgid):
			self.__msgid_indx += 1
			return self.__msgid_last+"-"+str(self.__msgid_indx)
		else:
			self.__msgid_indx = 1
			self.__msgid_last = msgid
			return msgid + "-1"

	def messageEntered(self):
		text = self.ui.lineEdit_input.text()
		msgid = self.generateMsgid()
		self.ui.lineEdit_input.setText("")
		self.ui.chatStack.currentWidget().appendMessage(msgid, {"type" : "text", "text" : str(text), "owner-name" : ""})
		data = {'type' : 'sendmessage', 'args' : {'to' : self.__contact_actual, 'text' : str(text), 'id' : msgid}}
		self.queueData.put(data)
			
	def closeEvent(self, event):
		self._exitThread = True
		while (not self._exited):
			time.sleep(0.1)
		self.core.close()
		event.accept()
